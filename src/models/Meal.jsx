export default class Meal{
    constructor(name, recipe, nb_people, ingredients, image, rating, category){
        this.name = name;
        this.recipe = recipe;
        this.nb_people = nb_people;
        this.ingredients = ingredients;
        this.image = image;
        this.count = 0;
        this.isFavorite = false;
        this.rating = rating;
        this.category = category;     // { name : healthy, icon : healthy_icon}
    }
}

