export default class Category{
    constructor(name, icon){
        this.name = name;
        this.icon = icon;
    }
}

/*
[
    {
        name : healthy,
        icon : healthy_icon
    },
    {
        ...
    }
]
*/