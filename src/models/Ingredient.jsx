export default class Ingredient{
    constructor(name, quantity, mesure) {
        this.name = name;
        this.quantity = quantity;
        this.mesure = mesure;       // [gramme, litre, piece, ...] --> state global
    }
}