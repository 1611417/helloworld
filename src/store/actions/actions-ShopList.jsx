// Constantes qui déterminent le type d'action à effectuer dans le reducer

export const ITEM_ADDED = "ITEM_ADDED";
export const ITEM_DELETED = "ITEM_DELETED";
export const ITEM_ALL_DELETE = "ITEM_ALL_DELETE"

export const addItem = (itemToAdd) => 
(
    {
    type : ITEM_ADDED,
    itemToAdd : itemToAdd
});

export const removeItem = (itemToDelete) => 
(
    {
        type : ITEM_DELETED,
        itemToDelete : itemToDelete
    }
)

export const removeAllItem = (itemToAllDelete) =>
(
    {
        type : ITEM_ALL_DELETE,
        itemToAllDelete : itemToAllDelete
    }
)