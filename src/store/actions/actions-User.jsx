export const SET_ACTIVE_USER = "SET_ACTIVE_USER";
export const ADD_FAVORITES = "ADD_FAVORITES";
export const REMOVE_FAVORITES = "REMOVE_FAVORITES"

export const setActiveUser = (user) => 
(
    {
        type : SET_ACTIVE_USER,
        payload : user
    }
)

export const addToFavorites = (mealId) => 
(
    {
        type : ADD_FAVORITES,
        payload : mealId
    }
)

export const removeFromFavorites = (mealId) => 
(
    {
        type : REMOVE_FAVORITES,
        payload : mealId
    }
)