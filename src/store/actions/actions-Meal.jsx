import axios from 'axios';

export const FETCH_MEAL = "FETCH_MEAL";
export const ON_LOADING_MEAL = "ON_LOADING_MEAL";
export const GET_URL_ID = "GET_URL_ID";


export const fetchMeal = (meals) =>
(
    {
        type : FETCH_MEAL,
        payload : meals
    }
)

export const onLoadingMeal = () => 
(
    {
        type : ON_LOADING_MEAL
    }
)

export const getUrlId = (mealId) => 
(
    {
        type : GET_URL_ID,
        payload : mealId
    }
)

export const loadMeal = () => 
{


    return (dispatch) => {
        dispatch(onLoadingMeal());

        axios.get('http://localhost:1617/api/meal')
            .then(({data}) => {
                console.log("fetching..")
                console.log(data)
                dispatch(fetchMeal(data));
            })
            .catch((e) => {
                console.log(e);
            });

    }
}