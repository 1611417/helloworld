import {SET_ACTIVE_USER, ADD_FAVORITES, REMOVE_FAVORITES} from '../actions/actions-User';

const initalstate = 
{
    activeUser : null,
    favorites : []
}

const UserReducer = (state = initalstate, action) => {
    switch(action.type)
    {
        case SET_ACTIVE_USER: return setActiveUser(state, action.payload);

        case ADD_FAVORITES: return addToFavorites(state, action.payload);

        case REMOVE_FAVORITES: return removeFromFavorites(state, action.payload);

        default:
            return state;
    }
}

function setActiveUser(state, user) {
    state = {
        ...state,
        activeUser : user,

    }
    return state;
}

function addToFavorites(state, mealId){
    state = {
        ...state,
        favorites : [...state.favorites, mealId]
    };
    return state;
}

function removeFromFavorites(state, mealId){
    state = {
        ...state,
        favorites : state.favorites.filter( (i) => i !== mealId)
    };
    return state;
}

export default UserReducer;