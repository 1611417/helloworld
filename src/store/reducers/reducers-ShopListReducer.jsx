import { ITEM_ADDED, ITEM_DELETED, ITEM_ALL_DELETE} from '../actions/actions-ShopList.jsx';

//valeur initiale du state ==> à modifier par le json une fois qu'il sera créé

const initalstate = 
{
    items : []
}

//fonction reducer

const ShopListReducer = (state = initalstate, action) =>
{
    switch(action.type)
    {
        //Ajouter à la shoplist
        case ITEM_ADDED : return addItem(action.itemToAdd, state);
        //Supprimer de la shoplist
        case ITEM_DELETED : return removeItem(action.itemToDelete, state);
        //Vider la shoplist
        case ITEM_ALL_DELETE : return removeAllItem(state)
        default:
            return state;
    }
}

// fonctions

function addItem(item, state) {
    state = {
        ...state,
        items : [...state.items, item ]
    }
    console.log("ADDING")
    return state;
}

function removeItem(item, state) {
    state = {
        ...state,
        items : state.items.filter( (i) => i !== item)
    }
    console.log("removeItem")
    console.log(item)
    return state;
}

function removeAllItem(state) {
    state = {
        ...state,
        items : []
    };
    console.log('removeAllItem')
    console.log(state.items)
    return state;
}

export default ShopListReducer;