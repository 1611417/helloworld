import {FETCH_MEAL, ON_LOADING_MEAL, GET_URL_ID} from '../actions/actions-Meal.jsx';


const initialstate = 
{
    meals : [],
    onLoading : false,
    urlId : 0 ,
    
}

const MealReducer = (state = initialstate, action ) => 
{
    switch(action.type) {
        // ...
       
        case ON_LOADING_MEAL : return {...state, onLoading: true};
        /// ...
        case FETCH_MEAL : return fetchMeal(action.payload, state);
        case GET_URL_ID : return getId(action.payload, state);
        default:
            return state;
    }
}


// fonctions


const fetchMeal = (meals, state) => {
    state= {
        ...state, 
        meals : meals,
        onLoading : false
    }
    return state;
}

const getId = (mealId, state) => {
    state = {
        ...state,
        urlId : mealId
    }
    return state;
}


export default MealReducer;