import {combineReducers} from 'redux' // Combiner les differents reducers en 1!
import ShopListReducer from './reducers/reducers-ShopListReducer.jsx'
import MealReducer from './reducers/reducer-MealReducer.jsx';
import UserReducer from './reducers/reducer-UserReducer.jsx';

export default combineReducers
(
    {
        ShopListReducer, 
        MealReducer,
        UserReducer
    }
)