import ListGroup from 'react-bootstrap/ListGroup'
import styles from '../ShopList/ShopList.module.css'
import Button from 'react-bootstrap/Button'

const ShopList = function(props){
    return(
        <div className={styles.shoplist}>
            <ListGroup>
                <ListGroup.Item variant="light">Poulet</ListGroup.Item>
                <ListGroup.Item variant="light">Oeufs</ListGroup.Item>
                <ListGroup.Item variant="light">Pain</ListGroup.Item>
                <ListGroup.Item variant="light">Lait</ListGroup.Item>
                <ListGroup.Item variant="light">Fromage</ListGroup.Item>
                <ListGroup.Item variant="light">Kellogs</ListGroup.Item>
                <ListGroup.Item variant="light">Nutella</ListGroup.Item>
                <Button variant="warning" className={styles.btn_shoplist}>
                    <i className="fas fa-shopping-cart"></i>
                </Button>
            </ListGroup>
        </div>
    )
}

export default ShopList;
