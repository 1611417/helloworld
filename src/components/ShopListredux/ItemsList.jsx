import React from 'react'
import { connect } from 'react-redux';
import {removeItem} from '../../store/actions/actions-ShopList';
const ItemsList = (props) => {

    return (
        <div>
          {props.list.map( (i) => 
         <p> Ingredient : {i.name}  |  Quantité : {i.quantity} </p>
          )}
        </div>
    )
}

const mapStateToProps = (state) =>
(
    {
        list : state.ShopListReducer.items
    }
);

const mapDispatchToProps = 
{
    remove : removeItem
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList)
