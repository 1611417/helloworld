import React from 'react'
import Formrcc from './FormRcc.jsx';
import ItemsList from './ItemsList.jsx';


export default function ShopListParent() {
    return (
        <div>
            <Formrcc />
            <ItemsList />
        </div>
    )
}
