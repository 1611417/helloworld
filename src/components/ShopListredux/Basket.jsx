import ListGroup from 'react-bootstrap/ListGroup'
import styles from '../ShopList/ShopList.module.css'
import Button from 'react-bootstrap/Button'
import { removeItem, removeAllItem } from '../../store/actions/actions-ShopList'
import { connect } from 'react-redux'
import emailjs from 'emailjs-com';
import {useState} from 'react'

const Basket = (props) => {
    const {list} = props;
    const {removeItem} = props;
    const {activeUser} = props;

   
    let test1 = []
    const test = list.map(function(i){
        i.name = i.name.toString();
        i.mes = i.mes.toString();
        i.quant = i.quant.toString();
        test1 = [...test1, i.name +'    '+ i.quant + ' ' + i.mes]
        return {
            test1
        }
    });
    let test2 = test1.join(' - / - ')
    
    

    function sendEmail(e) {
        e.preventDefault();
                
        const templateParams = {
            username: activeUser.username,
            message: test2,
            email: activeUser.email,
          };
    
        emailjs.send('service_ce0595q', 'template_9g3uzgf', templateParams, 'user_2ZUKQat9Lv84gyfYtLyN4')
          .then((result) => {
              console.log(result.text);
          }, (error) => {
              console.log(error.text);
          });
          props.removeAllItem(list);
      }

    return(
        <div className={styles.shoplist}>

            <ListGroup>
                {props.list.map( (i) => 
                <ListGroup.Item variant="light" key={i.name}> {i.name} <span className={styles.basket_qtt}>{i.mes}{i.quant} 
                <button className={styles.btnRemove} variant="outline-warning" onClick={ (e) => removeItem(i, list) }>
                <i class="fas fa-times"></i>    
                </button></span> </ListGroup.Item>
                )}
                <Button variant="warning" className={styles.btn_shoplist} type="submit" onClick={(e) => {sendEmail(e)}}>
                    <i className="fas fa-shopping-cart"></i>
                </Button>
            </ListGroup>

                


        </div>
    )
}

const mapStateToProps = (state) => 
(
    {
        list : state.ShopListReducer.items,
        activeUser : state.UserReducer.activeUser
    }
)

const mapDispatchToProps =
{
    removeItem : removeItem,
    removeAllItem : removeAllItem
}

export default connect(mapStateToProps, mapDispatchToProps)(Basket);