import React, { Component } from 'react'
import { connect } from 'react-redux';
import Ingredient from '../../models/Ingredient';
import {addItem} from '../../store/actions/actions-ShopList'
import ItemsList from './ItemsList';

class Formrcc extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             name : "",
             quantity : 0,
             mesure : 0
             
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleQuantityChange = this.handleQuantityChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this)
    }

    onSubmit(e) {
        e.preventDefault();
        //console.log(this.props.Data)
        let new_item = new Ingredient(this.state.name, this.state.quantity, this.state.mesure);
        this.props.pushToItems(new_item)
        this.setState ( 
            {
                name : "",
                quant : 0,
                mes : 0
            }
        )
    };

    handleNameChange(e) {
        this.setState ( 
            {
                name : e.target.value
            }
        )
    }

    handleQuantityChange(e) {
        this.setState( { quant : e.target.value})
    }

    handlePriceChange(e) {
        this.setState( { mes : e.target.value})
    }


    
    render() {
        const {name, quantity, mesure} = this.state;
        return (
            <div>
                <form className = "mt-5" onSubmit={this.onSubmit}>
                <div>
                <label for="name_field">Nom: </label>
                <input id="name_field" type="text" name = "name" value={name}
                onChange= {this.handleNameChange} />
                <label for="quantity_field"> Quantité: </label>
                <input type="number" id="quantity_field" name="quantity_field" value={quantity}
                onChange={this.handleQuantityChange}/>
                <label for="price_field">Mesure :</label>
                <input type="number" id="price_field" name="price_field" value={mesure}
                onChange={this.handlePriceChange} />
                </div>
                <button type="submit">Submit</button>
            </form>
            </div>
        )
    }
}

// const mapStateToProps = (state) => (
//     {
//         Data : state.ShopListReducer.items
//     }
// )

const mapDispatchToProps = 
{
    pushToItems : addItem
}

export default connect(null, mapDispatchToProps)(Formrcc);

