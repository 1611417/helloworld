import styles from '../Home/Home.module.css'
import Carousel from 'react-bootstrap/Carousel'
import RecetteGalerie from '../RecetteGalerie/RecetteGalerie'
import Row from 'react-bootstrap/Row'
import { connect } from 'react-redux'
import {loadMeal} from './../../store/actions/actions-Meal'
import React, {useEffect} from 'react';
import { addToFavorites } from '../../store/actions/actions-User'

const Home = function(props){
    const {meals, onLoading, loadMeal, favorites, addToFavorites} = props;
    const {activeUser} = props;

    useEffect(() => {
        setTimeout(()=> {
            loadMeal();
            activeUser.favorites.forEach( (f) => {
                addToFavorites(f)
            });  
        }, 10)
    }, []);

    console.log("favorites : ");
    console.log(favorites);
    return(
        <div className="container">
            <h1 className={styles.title}>Welcome {activeUser.username}</h1>
            <p className={styles.title2}>to Hello World</p>
            <h2 className={styles.title3}>Recettes au hasard</h2>
            <Carousel controls="true" className={styles.carousel_frame}>
                <Carousel.Item activeIndex="3">
                    <Row className={styles.carousel_items}>
                    {meals.slice(0,3).map((recette) => 
                    <RecetteGalerie meal={recette} /> 
                    )}
                    </Row>
                </Carousel.Item>
                <Carousel.Item activeIndex="3">
                    <Row className={styles.carousel_items}>
                    {meals.slice(3,6).map((recette) => 
                    <RecetteGalerie meal={recette} /> 
                    )}
                    </Row>
                </Carousel.Item>
            </Carousel>
            <div className={styles.waves_bottom}>
            </div>
        </div>
    )
}

const mapStateToProps  = (state) => ({
    meals : state.MealReducer.meals,
    onLoading: state.MealReducer.onLoading,
    activeUser : state.UserReducer.activeUser,
    favorites : state.UserReducer.favorites

})


const mapDispatchToProps = {
        loadMeal,
        addToFavorites : addToFavorites
}


export default connect(mapStateToProps,mapDispatchToProps)(Home)