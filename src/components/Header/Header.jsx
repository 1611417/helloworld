import styles from '../Header/Header.module.css'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Button from 'react-bootstrap/Button'
import logo from '../../data/img/logo.png'
import {NavLink} from 'react-router-dom'
import { setActiveUser } from '../../store/actions/actions-User';
import {connect} from 'react-redux';

const Navigation = function(props){

    const {setActiveUser} = props;

    const logOut = (e) => {
        setActiveUser(null);
    }

    return( 
    <Navbar className={styles.header} variant="dark" expand="lg">
        <Navbar.Brand>
            <NavLink to="/home"><img src={logo} height="90" alt="logo" className={styles.logo}></img>
            </NavLink>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
         <Nav className="mr-auto">
         <div className={styles.nav_link}>
          <NavLink to="/home">Home</NavLink><span>|</span>
          <NavLink to="/about">About</NavLink><span>|</span>
          <NavLink to="/recettes">Recettes</NavLink><span>|</span>
          <NavLink to="/favorites">Favorites</NavLink>
        </div>
        </Nav>
        <Nav className={styles.buttons}>
            <NavLink to="/addrecette" className={styles.btn_header}>
                <Button variant="warning">
                    <i className="fas fa-plus "></i>
                </Button>
            </NavLink>
            <NavLink to="/login" className={styles.btn_header}>
                <Button variant="warning" onClick = {(e) =>logOut(e)}>
                <i class="fas fa-sign-out-alt"></i>
                </Button>
            </NavLink>
        </Nav>
        </Navbar.Collapse>
    </Navbar>
    );
}


const mapStateToProps = (state) =>
(
    {
        activeUser : state.UserReducer.activeUser
    }
)

const mapDispatchToProps =
{
    setActiveUser : setActiveUser
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigation)