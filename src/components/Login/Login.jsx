import styles from '../Login/Login.module.css'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import {useState, useRef} from 'react';
import axios from 'axios';
import { setActiveUser } from '../../store/actions/actions-User';
import {connect} from 'react-redux';
import { NavLink } from 'react-router-dom'


var Scroll = require('react-scroll');

const Login = function(props){
    const [registerUsername, setRegisterUsername] = useState("");
    const [registerEmail, setRegisterEmail] = useState("");
    const [registerPassword, setRegisterPassword] = useState("");

    const [loginUsername, setLoginUsername] = useState("");
    const [loginEmail, setLoginEmail] = useState("");
    const [loginPassword, setLoginPassword] = useState("");

    const [user, setUser] = useState(null);

    const {activeUser} = props;
    const {setActiveUser} = props;

    var scroll = Scroll.animateScroll;

    // Gère l'envoi verts le backend
    const register = (e) => {
        e.preventDefault();
        axios({
            method : "post",
            data: {
                username : registerUsername,
                email : registerEmail,
                password : registerPassword
            },
            withCredentials : true,
            url : "http://localhost:1617/api/register" 
        }).then((res) => console.log(res))
        .catch(err => console.log(err))
        setRegisterUsername('')
        setRegisterEmail('')
        setRegisterPassword('')

    };

    // async function loginCb(res) {
    //     await setUser(user);
    //     if (activeUser){

    //     }
    // }
    
    const login = (e) => {
        
        axios({
            method : "post",
            data: {
                username : loginUsername,
                password : loginPassword
            },
            withCredentials : true,
            url : "http://localhost:1617/connect" 
        }).then((res) => {
            console.log(res.data);
            if(res.data !== "Invalid email"){
            setActiveUser(res.data);}
        }) //console.log(res.data)
        .catch(err => console.log(err));

        //await setActiveUser(activeUser, user);
    };
    const getUser = () => {
        axios({
            method : "get",
            withCredentials : true,
            url : "http://localhost:1617/user"
        }).then((res) => {
            console.log(res.data);
            setActiveUser(res.data);
        })
        // console.log("Active user : ")
        // console.log(activeUser);

    };

    const logOut = (e) => {
        // axios({
        //     method : "get",
        //     withCredentials : true,
        //     url : "http://localhost:1617/logout"
        // }).then(console.log(activeUser))
        console.log("Avant Logout:")
        console.log(activeUser);
        setActiveUser(null);
    }

    return(
        <div className="container">
            <div className={styles.login_page}>
            <Row>
                <Col></Col>
                <Col md="auto">
                    <i class="far fa-user-circle fa-9x"></i><br/><br/>
                    <h1>Log in</h1>
                </Col>
                <Col></Col>
            </Row>   
            <Row className={styles.login_form}>
                <Col></Col>
                <Col>
                
                
               
                <Form.Group controlId="formLoginUsername">
                        <Form.Label>User name</Form.Label>
                        <Form.Control type="text" placeholder="Enter user name" 
                         onChange={(e) => setLoginUsername(e.target.value)}/>
                    </Form.Group>
                    {/* <Form.Group controlId="formLoginEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" 
                         onChange={(e) => setLoginEmail(e.target.value)}/>
                    </Form.Group> */}
                    <Form.Group controlId="formLoginPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" 
                        onChange={(e) => setLoginPassword(e.target.value)}/>
                    </Form.Group>
                    <div className={styles.login_btn}>

                    <NavLink to={"/home"}>
                    <Button variant="warning" className={styles.btnLogin} type="submit" onClick={(e) => login(e)}> 
                        Login
                    </Button>
                    </NavLink>
                    <Button variant="warning" className={styles.btnLogin} onClick={() => scroll.scrollToBottom()}>Register</Button>
                    
                    </div>
                </Col>
                <Col></Col>
            </Row>

            {//
            //<Button variant="warning" onClick = {getUser}>Get USer</Button>
            //<Button variant="warning" onClick = {(e) =>logOut(e)}>logOUt</Button>
            //<Button variant="warning" onClick = {(e) => {
            //    console.log("Current user status :");
            //    console.log(activeUser)
            //}}>GET STATUS</Button>
            }
            </div>
            <div className={styles.register_page}>


            <Row>
                <Col></Col>
                <Col md="auto">
                    <div className={styles.iconCenter}>
                    <i class="far fa-user-circle fa-9x"></i>
                    </div><br/><br/>
                    <h1>Register</h1>
                </Col>
                <Col></Col>
            </Row>   
            <Row className={styles.login_form}>
                <Col></Col>
                <Col>
                <Form>
                <Form.Group controlId="formRegisterUsername">
                        <Form.Label>User Name</Form.Label>
                        <Form.Control type="text" value={registerUsername} placeholder="Enter username" 
                        onChange={(e) => setRegisterUsername(e.target.value)}/>
                    </Form.Group>
                    <Form.Group controlId="formRegisterEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" value={registerEmail} placeholder="Enter email" 
                        onChange={(e) => setRegisterEmail(e.target.value)}/>
                    </Form.Group>
                    <Form.Group controlId="formRegisterPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={registerPassword} placeholder="Password" 
                         onChange={(e) => setRegisterPassword(e.target.value)}/>
                    </Form.Group>
                    <div className={styles.login_btn}>
                    <Button variant="warning" className={styles.btnLogin} type="submit" onClick={(e) => register(e)}>
                        Register
                    </Button>
                    <Button variant="warning" className={styles.btnLogin} type="submit" onClick={() => scroll.scrollToTop()}> 
                        Login
                    </Button>
                    </div>
                    </Form>
                </Col>
                <Col></Col>
            </Row>
            {
            //<div className={styles.waves}>
            //<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#98c1d9" fill-opacity="1" d="M0,96L120,106.7C240,117,480,139,720,138.7C960,139,1200,117,1320,106.7L1440,96L1440,320L1320,320C1200,320,960,320,720,320C480,320,240,320,120,320L0,320Z"></path></svg>
            //</div>
            }
        </div>
        </div>
    )
}

const mapStateToProps = (state) =>
(
    {
        activeUser : state.UserReducer.activeUser
    }
)

const mapDispatchToProps =
{
    setActiveUser : setActiveUser
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)