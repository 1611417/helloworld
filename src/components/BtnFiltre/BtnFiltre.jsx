import styles from './BtnFiltre.module.css'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'

const BtnFiltre = function(props){
return(
    <div>
        <ul>
            <li><Button variant="warning" className={styles.btn_filtre} >Healthy <i className="fas fa-leaf"></i></Button></li>
            <li><Button variant="warning" className={styles.btn_filtre} >Poisson <i class="fas fa-fish"></i></Button></li>
            <li><Button variant="warning" className={styles.btn_filtre} >Viande <i class="fas fa-drumstick-bite"></i></Button></li>
            <li><Button variant="warning" className={styles.btn_filtre} >Enfants <i class="fas fa-baby"></i></Button></li>
            <li><Button variant="warning" className={styles.btn_filtre} >Asiat <i class="fas fa-globe-asia"></i></Button></li>
            <li><Button variant="warning" className={styles.btn_filtre} >Unhealthy <i class="fas fa-hamburger"></i></Button></li>
        </ul>
        <Form inline>
          <FormControl type="text" placeholder="Filtre" className={styles.input_filtre}/>
        </Form>
    </div>
)
}

export default BtnFiltre