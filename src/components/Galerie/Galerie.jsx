import React, {useEffect, useState} from 'react';

import RecetteGalerie from '../RecetteGalerie/RecetteGalerie'

import Row from 'react-bootstrap/Row'
import styles from '../Galerie/Galerie.module.css'
import Button from 'react-bootstrap/Button'
import { connect } from 'react-redux'
import {loadMeal} from './../../store/actions/actions-Meal'

const Galerie = function(props){
    const {meals, onLoading, loadMeal} = props;
    const [activeFilters, setActiveFilters] = useState([])
    const [filteredMeals, setFilteredMeals] = useState([]);
    
    let buff = [];
    

    useEffect(() => {
        setTimeout(()=> {
            loadMeal();  
        }, 1000)
    }, []);

    useEffect(() => {
        setFilteredMeals(meals);
    }, [onLoading])

    useEffect(() => {
        filterFunction();
        console.log("filteredMeals Updtate")
    }, [activeFilters])


   const filterFunction = () => {
       buff = [];
       if(activeFilters.length === 0){
          buff = meals;
          console.log("filteredMeals is empty")
       }
       else {
           activeFilters.forEach( t => {
               meals.forEach ( m => {
                   m.category.forEach ( c => {
                       if(c.catName === t && !(buff.includes(m))) {
                            buff.push(m)
                       }
                   })
               })
           })
       }
    

       console.log("Pre stack check");
       console.log(buff);

       let toRemove = [];
       let catBuff = []

       activeFilters.forEach( tag => {
           buff.forEach( meal => {
               catBuff = [];
               meal.category.forEach( c => {
                   catBuff.push(c.catName)
               })
               if(!(catBuff.includes(tag)) && !(toRemove.includes(meal))){
                   toRemove.push(meal)
               }
           })
       })

       console.log("toRemove");
       console.log(toRemove);


       buff = buff.filter( function( el ) {
        return !toRemove.includes( el );
      } );

       console.log("Post stack check");
       console.log(buff);
       setFilteredMeals(buff);
   }
    

    // const handleClick = (e) => { 
    //     if(activeFilters.includes(e.target.value)){
    //         setActiveFilters(
    //             activeFilters.filter( tag => {return (tag!== e.target.value)})
    //         )
    //     }
    //     if(!activeFilters.includes(e.target.value)) {
    //     setActiveFilters([...activeFilters, e.target.value])
    //     }
    // }

    // HEALTHY

    const handleClickHealthy = (e) => { 
        if(activeFilters.includes("Healthy")){
            setActiveFilters(
                activeFilters.filter( tag => {return (tag !== "Healthy")})
            )
        }
        if(!activeFilters.includes("Healthy")) {
        setActiveFilters([...activeFilters, "Healthy"])
        }
    }

    //POISSON

    const handleClickPoisson = (e) => { 
        if(activeFilters.includes("Poisson")){
            setActiveFilters(
                activeFilters.filter( tag => {return (tag!== "Poisson")})
            )
        }
        if(!activeFilters.includes("Poisson")) {
        setActiveFilters([...activeFilters, "Poisson"])
        }
    }

    //ENFANTS

    const handleClickEnfants = (e) => { 
        if(activeFilters.includes("Enfants")){
            setActiveFilters(
                activeFilters.filter( tag => {return (tag!== "Enfants")})
            )
        }
        if(!activeFilters.includes("Enfants")) {
        setActiveFilters([...activeFilters, "Enfants"])
        }
    }

    // ASIAT

    const handleClickAsiat = (e) => { 
        if(activeFilters.includes("Asiat")){
            setActiveFilters(
                activeFilters.filter( tag => {return (tag!== "Asiat")})
            )
        }
        if(!activeFilters.includes("Asiat")) {
        setActiveFilters([...activeFilters, "Asiat"])
        }
    }

    //UNHEALTHY

    const handleClickUnhealthy = (e) => { 
        if(activeFilters.includes("Unhealthy")){
            setActiveFilters(
                activeFilters.filter( tag => {return (tag!== "Unhealthy")})
            )
        }
        if(!activeFilters.includes("Unhealthy")) {
        setActiveFilters([...activeFilters, "Unhealthy"])
        }
    }

    //VIANDE
    const handleClickViande = (e) => { 
        if(activeFilters.includes("Viande")){
            setActiveFilters(
                activeFilters.filter( tag => {return (tag!== "Viande")})
            )
        }
        if(!activeFilters.includes("Viande")) {
        setActiveFilters([...activeFilters, "Viande"])
        }
    }


    const resetFilters = (e) => {
        setActiveFilters([]);
    }


    return(
        <div>
            {/* BtnFiltre*/}
            {/*
            <button onClick={(e) => {
                console.log("active filters");
                console.log(activeFilters);
            }}>FILTERS </button>
            */}
            <ul>
            <li><Button variant="warning" className={styles.btn_filtre} onClick={(e) => resetFilters(e) } >All </Button></li>
            <li><Button variant="warning" className={styles.btn_filtre} onClick={(e) => handleClickHealthy(e)}>Healthy <i className="fas fa-leaf"></i></Button></li>
            <li><Button variant="warning" className={styles.btn_filtre} onClick={(e) => handleClickPoisson(e)} >Poisson <i class="fas fa-fish"></i></Button></li>
            <li><Button variant="warning" className={styles.btn_filtre} onClick={(e) => handleClickViande(e)}>Viande <i class="fas fa-drumstick-bite"></i></Button></li>
            <li><Button variant="warning" className={styles.btn_filtre} onClick={(e) => handleClickEnfants(e)} >Enfants <i class="fas fa-baby"></i></Button></li>
            <li><Button variant="warning" className={styles.btn_filtre} onClick={(e) => handleClickAsiat(e)}>Asiat <i class="fas fa-globe-asia"></i></Button></li>
            <li><Button variant="warning" className={styles.btn_filtre} onClick={(e) => handleClickUnhealthy(e)} >Unhealthy <i class="fas fa-hamburger"></i></Button></li>
        </ul>

        {/* Galerie */}
        {onLoading ? (
            <h1>Chargement...</h1>
        ) : (
            
            <Row className={styles.row_centering}>
            {filteredMeals.map((recette) =>
                <RecetteGalerie meal={recette}  />   //recette.id
            )}
            </Row>
        )}
        </div>
    )
}

// Regle pour Redux => Mapping du state du store vers les props du composant
const mapStateToProps  = (state) => ({
        meals : state.MealReducer.meals,
        onLoading: state.MealReducer.onLoading
})

// Regle pour Redux => Mapping des méthodes d'action vers les props du composant
const mapDispatchToProps = {
        loadMeal
}

// connect => Permet de créer un composant parent avec les regles pour lier le store
export default connect(mapStateToProps, mapDispatchToProps)(Galerie)