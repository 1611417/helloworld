import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import ListGroup from 'react-bootstrap/ListGroup'
import styles from '../RecetteAffiche/RecetteAffiche.module.css'
import { connect } from 'react-redux'
import {useState, useEffect} from 'react';
import {addItem} from '../../store/actions/actions-ShopList'
import { addToFavorites, removeFromFavorites } from '../../store/actions/actions-User'
import axios from 'axios';

const RecetteAffiche = function(props){
    
    const [toggleFavorite, setToggleFavorite] = useState(false);

    const meals = props;
    let cpt = 0;
    
   console.log("activeuser");
   console.log(props.activeUser)

    

   const id = props.match.params.id;

   useEffect(() => {
    axios({
        method : "put",
        data: {
            favorites : props.favorites
        },
        withCredentials : true,
        url : `http://localhost:1617/api/favorites/${props.activeUser._id}`
    })

    .then(console.log(props.favorites))

    }, [props.favorites])

   
    const isFavorite = () => {
        let control = false
        props.favorites.forEach( meal => {
        if(meal === id){
            control = true;
        }
        });
        return control;
    }
        

    const fetchRecipe = meals.meals.filter(function(recette){
        if(recette._id === id){
        return {
        recette
        }}
        });


    
    console.log("RECIPE:")
    console.log(fetchRecipe);
    console.log("H1");
    console.log(fetchRecipe[0].name)

    //setRecipe(thisRecipe);

    // useEffect(() => {
    //     console.log("Use Effect :"+recipe)
    // }, [recipe])

    const [countPersonnes, setcountPersonnes] = useState(fetchRecipe[0].nb_people);
    const [ingredients, setIngredients] = useState([]);
    const [initalList, setInitialList] = useState([])

    useEffect(async () => {
    let fetchRecipe = await meals.meals.filter(function(recette){
            if(recette._id === id){
            return {
            recette
            }}
            });
        setIngredients(handleIngr());
        setInitialList(handleIngr());
    }, []);

    const handleIngr = () => {
        let bufObject = [];
        let ingre = [];
        fetchRecipe[0].ingredients[0].forEach(  (i) => {
           bufObject = [i.name, i.mes, i.quant];
           ingre = [...ingre, bufObject];
    
    
       })
       return ingre;
    };

    const handleFavorite = (e) => {
        if(toggleFavorite === false){
            setToggleFavorite(true);
        } else {
            setToggleFavorite(false)
        }
        if (isFavorite()){
            props.removeFromFavorites(id);
        } else {
            props.addToFavorites(id);
        }
    };

    const regledeTrois = () => {

        let bufIngredients = [];
        let cpt = 0;
        ingredients.forEach( (i) => {
            bufIngredients = [...bufIngredients, [ i[0], i[1] ,( initalList[cpt][2] / fetchRecipe[0].nb_people) * countPersonnes  ]]
            cpt = cpt + 1;
        })
        console.log("cpt" + cpt)
        console.log("bufIngredients");
        console.log(bufIngredients);
        return bufIngredients;
    } 

    useEffect(() => {
        setIngredients(regledeTrois());
    }, [countPersonnes])



    
    const personnePlusUn = (e) => {
        setcountPersonnes(countPersonnes+1);
       
    }

    const personneMoinsUn = (e) => {
        setcountPersonnes(countPersonnes-1);
    }



    return(
        <div className="container">
            <Container>
                <Row className={styles.recette_titre}>
                    <Col>
                    <h2>{fetchRecipe[0].name}</h2>
                    </Col>
                </Row>
                <Row>
                    <Col lg={8}></Col>
                    <Col lg={3}>
                    <div className={styles.frame_nbPersonnes}>
                            
                            <span className={styles.nbPersonnes}>Personnes :</span>
                            <span className={styles.nbPersonnesNumber}>{countPersonnes}</span>

                            <Button variant="warning" className={styles.btn_nbPersonnes} onClick={(e) => personnePlusUn(e)}>
                                <i className="fas fa-chevron-up fa-xs"></i>
                            </Button>

                            <Button variant="warning" className={styles.btn_nbPersonnes} onClick={(e) => personneMoinsUn(e)}>
                                <i className="fas fa-chevron-down fa-xs"></i>
                            </Button>
                        </div>
                    </Col>
                    <Col lg={1}></Col>
                </Row>
                <Row>
                    <Col lg={8}>
                        
                        <img src={fetchRecipe[0].image} alt="img" className={styles.mealImage}/>
                        <ul>
                        <li>
                        <Button variant="warning" className={styles.btn_shoplist} onClick = { (e) => {
                            ingredients.forEach( (i) =>{ 
                                console.log("ingredient shoplist");
                                console.log(i);
                                props.addItem({name : i[0], quant : i[1], mes : i[2]})})
                                console.log("shoplist");
                                console.log(props.items)
                                
                            //props.addItem(fetchRecipe[0].ingredients[0], props.list)
                        }} >  
                            <i className="fas fa-shopping-cart"></i>
                        </Button>
                        </li>
                        <li>
                        <Button variant="warning" className={styles.btn_shoplist} onClick={(e) => handleFavorite(e)} >
                            <i className="fas fa-heart"></i>
                        </Button>
                        </li>
                        </ul>
                        <ul className={styles.filter_type}>
                        {fetchRecipe[0].category.map((cat) =>
                        <li key={cat._id}><i className={cat.className}></i></li>)}
                        </ul>
                        <br></br>
                        {//
                        //<ul className={styles.rating}>
                        //<Rating
                        //    emptySymbol="fa fa-star-o fa-2x"
                        //    fullSymbol="fa fa-star fa-2x"
                        //   >
                        //</ul>
                        }
                    </Col>
                    <Col lg={4}>

                    <ListGroup>

                        {/** Map les ingrédients présents dans la recette // Affiche les données en fonction des éléments présents*/}

                        {ingredients.map((i) =>
                        <ListGroup.Item>
                            <Row>
                                <Col lg={8}>
                                <span className={styles.ingredientsName}>{i[0]}</span>
                                </Col>
                                <Col lg={2}>
                                    <span className={styles.ingredientsQtt}>{i[2]}</span> 
                                </Col>
                                <Col lg={2}>
                                    <span className={styles.ingredientsMesure}>{i[1]}</span>
                                </Col>
                            </Row>
                            
                            
                            
                        </ListGroup.Item>
                        )}

                    </ListGroup>
                    </Col>
                </Row>

                <Row className={styles.recipeFrame}>
                    <Col md={12}>
                    <h1>Recette :</h1>
                    <ListGroup>
                    {fetchRecipe[0].recipe.map((recipe) =>
                    <ListGroup.Item><h4>Partie {cpt=cpt+1}:</h4><p>{recipe}</p></ListGroup.Item>)}
                    </ListGroup>
                    </Col>
                </Row>
            </Container>            
        </div>
    )
}

const mapStateToProps = (state) => 
(
    {
        meals : state.MealReducer.meals,
        items : state.ShopListReducer.items,
        activeUser : state.UserReducer.activeUser,
        favorites : state.UserReducer.favorites
    }
)

const mapDispatchToProps =
{
    addItem : addItem,
    addToFavorites : addToFavorites,
    removeFromFavorites : removeFromFavorites
}

export default connect(mapStateToProps, mapDispatchToProps)(RecetteAffiche);