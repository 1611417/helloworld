
import styles from '../Favorites/Favorites.module.css'
import { connect} from 'react-redux'
import Row from 'react-bootstrap/Row'
import RecetteGalerie from '../RecetteGalerie/RecetteGalerie'
import {useState, useEffect} from 'react'
import Col from 'react-bootstrap/Col'

const Favorites = function(props){

    const {favorites, meals} = props;
    const [filteredMeals, setFilteredMeals] = useState([]);

//    const filterFavorites = () => {
//        /* 
//         for m in meals:
//             for id in favorites:
//                 if (m._id == id):
//                     return m

//       */
//       let uwu = [];
//       meals.forEach( m => {
//           favorites.forEach( id => {
//               if(m._id === id){
//                   uwu = [...uwu, m]
//               }
//           } )
//       })
//       return uwu;
//    }

   useEffect(() => {
      // setFilteredMeals(filterFavorites());
      setFilteredMeals( () => {
        let uwu = [];
        meals.forEach( m => {
            favorites.forEach( id => {
                if(m._id === id){
                    uwu = [...uwu, m]
                }
            } )
        })
        return uwu;}
      )
      
   }, [])

    return(
        <div className="container">
            <Row>
                <Col></Col>
                <Col>
                <h1 className={styles.title}>Mes Favoris</h1>
                </Col>
                <Col></Col>
                
            </Row>
                <Row className={styles.row_centering}>
                {filteredMeals.map((recette) =>
                <RecetteGalerie meal={recette} key={recette._id} />   //recette.id
                )}
                </Row>
        </div>
    )
    }


const mapStateToProps = (state) => 
(
    {
        favorites : state.UserReducer.favorites,
        meals : state.MealReducer.meals
    }
)


export default connect(mapStateToProps, null)(Favorites);