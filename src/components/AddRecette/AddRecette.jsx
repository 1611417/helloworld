import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import styles from '../AddRecette/AddRecette.module.css'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import ListGroup from 'react-bootstrap/ListGroup'
import { useState } from 'react'

import axios from 'axios'



const AddRecette = function(props){
    const [name, setName] = useState("");
    const [recipe, setRecipe] = useState("");
    const [recipes, setRecipes] = useState([]);
    const [people, setPeople] = useState(2);
    const [image, setImage] = useState('');
    const [imageMeal, setImageMeal] = useState("");
    const [ingredients, setIngredients] = useState([]);
    const [category, setCategory] = useState([]);
    const [ingredientName, setIngredientName] = useState("");
    const [mesure, setMesure] = useState("");
    const [quantity, setQuantity] = useState(0);
    const [buff, setBuff] = useState("");
    const [loading, setLoading] = useState(false);

    const handleCategory = (e) => {
        switch(buff){
            case ("Healthy"): 
                setCategory( [...category, ["Healthy", "fas fa-leaf"]] ); break;
            case ("Poisson") :
                setCategory([...category, ["Poisson", "fas fa-fish" ]]);
                break;
            case ("Viande"):
                setCategory([...category, ["Viande", "fas fa-drumstick-bite" ]]);
                break;
            case("Enfants"):
                setCategory([...category, ["Enfants", "fas fa-baby"]]);
                break;
            case ("Asiat"):
                setCategory([...category, ["Asiat", "fas fa-globe-asia"]]);
                break;
            case ("Unhealthy"):
                setCategory([...category, ["Unhealthy", "fas fa-hamburger"]]);
                break;
            default:
                break;          
        }
        setBuff("");
    }

    //const displayIngredients = () => ingredients.map( (i) => {<ListGroup.Item variant="light">{i.name} : {i.quant} {i.mes} </ListGroup.Item>});
    //const displayIngredients = () => console.log(ingredients);

    const addIngredient = (e) => {
        
        setIngredients([...ingredients, {
            name : ingredientName,
            mes : mesure,
            quant : quantity
        }]);
        setIngredientName("");
        setMesure("");
        setQuantity("");
    }

    const addRecipe = (e) => {

        setRecipes([...recipes, recipe]);
        setRecipe("");
    }

    const submit = (e) => {
        e.preventDefault();

        // console.log("Name : "+ name);
        // console.log("Recipe : "+ recipe);
        // console.log("People : "+ people);
        // console.log("Image : "+ image);
        // console.log("INgredients : "+ ingredients);
        // console.log("Category : ");
        // console.log(category);

        let catBuff = category.map( function(cat) {
            return(
            {
                catName : cat[0],
                className : cat[1]
            })
        })

        console.log(catBuff);

        let newMeal = { 
            name : name,
            recipe : recipes,
            category : catBuff,
            ingredients : ingredients,
            nb_people : people,
            image: imageMeal,
        };

        axios.post('http://localhost:1617/api/meal', newMeal)
        .then(() => {
            console.log("grand succès!");
            setName("");
            setRecipe("");
            setCategory([]);
            setIngredients([]);
            setPeople(2);
            setRecipes([]);
            setImageMeal('');
            setImage('');
        })
        .catch(console.log("Ca n'a pas vraiment atteint la cible"))
        
    }


    /*  Fonction  */

    const url = 'https://api.cloudinary.com/v1_1/a114between1617/image/upload';
    const preset = 'rsdgo2yu';

    const onSubmitImage = async () => {
        const formData = new FormData();
        formData.append('file', image);
        formData.append('upload_preset', preset);
        try {
          setLoading(true);
          const res = await axios.post(url, formData);
          const imageUrl = res.data.secure_url;
          console.log('url de retour ?')
          console.log(imageUrl)
          return setImageMeal(imageUrl);
        } catch (err) {
          console.error(err);
          console.log('NOT IN THE CLOUD')
        }
        setImage('');
      };

    return(
        <div className="container">
            <h1 className={styles.title}>Ajoutez vos recettes</h1>
            <Row>
                <Col className={styles.AddRecette_Form}>
                    <Form onSubmit={submit}>


                        <Form.Row>
                            <Col lg={12}>
                        <Form.Group controlId="Meal.Name">
                            <Form.Label>Nom du plat</Form.Label>
                            <Form.Control type="name" placeholder="Nom du plat" value={name}
                             onChange={ (e) => setName(e.target.value)}/>
                        </Form.Group>
                             </Col>
                        </Form.Row>


                        <Form.Row>
                            <Col lg={11}>
                            <Form.Group>
                            <input className={styles.inputfile} id="imgInput" type="file" name="image"  onChange={(e) => setImage(e.target.files[0])}/>
                            <label className={styles.btn_inputfile} for="imgInput"><i className="fas fa-file-upload"></i> Ajouter une image</label>
                            </Form.Group>
                            </Col>
                            <Col lg={1}>
                            <Button variant="warning" onClick={onSubmitImage}><i className="fas fa-plus"></i></Button>
                            </Col>
                        </Form.Row>

                    
                        <Form.Label>Categorie</Form.Label> 
                        <Form.Group controlId="Meal.Category">
                            <Form.Row> 
                                <Col lg={11}>
                                <Form.Control as="select" value={buff} 
                                onChange={(e) => setBuff(e.target.value)} >     
                            <option></option>
                            <option>Healthy</option>
                            <option>Poisson</option>
                            <option>Viande</option>
                            <option>Asiat</option>
                            <option>Enfants</option>
                            <option>Unhealthy</option>
                            </Form.Control>
                                </Col>
                                <Col lg={1}>
                                <Button variant="warning" className={styles.btn_addCategory} onClick={handleCategory}>
                            <i class="fas fa-plus"></i>
                            </Button>
                                </Col>
                            </Form.Row>

                            <Form.Row>
                                <Col lg={12}>
                            <ListGroup>
                                <ListGroup.Item>
                                {category.map((cat) => 
                                    
                                        <Button variant="warning" className={styles.btn_category}><i className={cat[1]}></i></Button>
                                )}
                                </ListGroup.Item>
                            </ListGroup>
                            </Col>
                            </Form.Row>
                        </Form.Group>
                        
                        <Form.Group controlId="Meal.Category">
                            <Form.Label>Nombre de personnes</Form.Label>
                            <Form.Control as="select"  value={people}
                            onChange={(e) => setPeople(e.target.value)}>
                            <option>2</option>
                            <option>4</option>
                            <option>6</option>
                            </Form.Control>
                        </Form.Group>
                        
                        <Form.Label>Ingrédients :</Form.Label>
                        <Form.Row>
                            <Col xs={7}>
                            <Form.Control placeholder="Ingredients..." value={ingredientName}
                            onChange={ (e) => setIngredientName(e.target.value)}/>
                            </Col>
                            <Col lg={2}>
                            <Form.Control placeholder="..." value={quantity}
                            onChange = {(e) => setQuantity(e.target.value)} />
                            </Col>
                            <Col lg={2}>
                            <Form.Control as="select" placeholder="Mesure" value={mesure}
                            onChange={(e)=>setMesure(e.target.value)}>
                            <option></option>
                            <option>Pc</option>
                            <option>Gr</option>
                            <option>Kg</option>
                            <option>Ml</option>
                            <option>Cl</option>
                            <option>L</option>
                            </Form.Control>
                            </Col>
                            <Col lg={1}>
                            <Button variant="warning"
                            onClick={addIngredient}>
                            <i class="fas fa-plus"></i>
                            </Button></Col>
                        </Form.Row>

                        <Form.Row>
                            <Col lg={11}>
                                <Form.Group controlId="Meal.Recipe">
                                    <Form.Label>Recette :</Form.Label>
                                    <Form.Control as="textarea" rows={3} value={recipe} 
                                    onChange={(e) => setRecipe(e.target.value)}/>
                                </Form.Group>
                            </Col>
                            <Col lg={1}>
                            <Button variant="warning" className={styles.btn_addRecipe} onClick={addRecipe}>
                                <i class="fas fa-plus"></i>
                            </Button>
                            </Col>
                            <ListGroup>
                            {recipes.map( (i) => <ListGroup.Item variant="light">{i} </ListGroup.Item>)}
                            </ListGroup>
                        
                        </Form.Row>
                        
                        {// Upload de l'image et récupération de l'url
                        }
                        <Form.Row>
                        <Button variant="warning" type="submit" className={styles.sendRecipe}>Envoyer la recette</Button>
                        </Form.Row>
                    </Form>
                </Col>
                <Col md={1}></Col>
                <Col className={styles.AddRecette_Liste}>
                <ListGroup>
                {ingredients.map( (i) => <ListGroup.Item variant="light">{i.name} : {i.quant} {i.mes} </ListGroup.Item>)}
                </ListGroup>
                </Col>
            </Row>
        </div>
    )
}

export default AddRecette