import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import ListGroup from 'react-bootstrap/ListGroup'
import styles from '../Historique/Historique.module.css'
import BtnFiltre from '../BtnFiltre/BtnFiltre'


const Historique = function(props){
    return(
        <div className="container">
            <Row>
                <Col></Col>
                <Col>
                    <h1 className={styles.historique_titre}>Historique</h1>
                </Col>
                <Col></Col>
            </Row>
            <Row>
                <Col>
                    <div className={styles.historique_filtre}>
                    <BtnFiltre/>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                <ListGroup variant="flush" className={styles.list_background}>
                    <ListGroup.Item>Recette ... en date du ../../....</ListGroup.Item>
                    <ListGroup.Item>Recette ... en date du ../../....</ListGroup.Item>
                    <ListGroup.Item>Recette ... en date du ../../....</ListGroup.Item>
                    <ListGroup.Item>Recette ... en date du ../../....</ListGroup.Item>
                    <ListGroup.Item>Recette ... en date du ../../....</ListGroup.Item>
                    <ListGroup.Item>Recette ... en date du ../../....</ListGroup.Item>
                    <ListGroup.Item>Recette ... en date du ../../....</ListGroup.Item>
                    <ListGroup.Item>Recette ... en date du ../../....</ListGroup.Item>
                    <ListGroup.Item>Recette ... en date du ../../....</ListGroup.Item>
                    <ListGroup.Item>Recette ... en date du ../../....</ListGroup.Item>
                    
                </ListGroup>
                </Col>
            </Row>
            <div className={styles.waves_bottom}>
            </div>
        </div>
    )
}

export default Historique