import Card from 'react-bootstrap/Card'
import imgRecette from '../../data/img/recette1.png'
import styles from '../RecetteGalerie/RecetteGalerie.module.css'
import Button from 'react-bootstrap/Button'
import {NavLink} from 'react-router-dom'
import { connect} from 'react-redux'
import {getUrlId} from '../../store/actions/actions-Meal'

const RecetteGalerie = function(props){
    
    
    
    const meal = props.meal;

    const categoryJSX = meal.category.map( 
        (cat) =>  <li key={cat._id}><i className={cat.className}></i></li>
    )
    

return(
    <Card style={{ width: '18rem' }} className={styles.card_spacing}>
        <Card.Img variant="top" src={meal.image} />
        <Card.Body>
        <Card.Title>{meal.name}</Card.Title>
            <Card.Text>
            <ul className={styles.filter_type}>
                {
                  categoryJSX  
                }
            </ul>
            </Card.Text>
            <div className={styles.btn_recette}>

            <NavLink to={`/recette/${meal._id}`} className={styles.btn_affiche}>

            <Button variant="warning" className={styles.btn_recette_size} >
                <i className="fas fa-eye"></i>
            </Button>
            
            </NavLink>
            </div>
        </Card.Body>
</Card>
)
}

const mapDispatchToProps = 
{
    getUrlId 
}

export default connect(null, mapDispatchToProps)(RecetteGalerie)