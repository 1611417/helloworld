import styles from '../Footer/Footer.module.css'


const Footer = function(props){
    return(
        <div className={styles.footer}>
            <p className={styles.footer_text}> Made with <br></br>React - Bootstrap - ExpressJs &amp; MongoDB <br></br>
            &copy; Roman Dhoore - {(new Date().getFullYear())}<br/>
            &copy; Cédric Nédostoupof - {(new Date().getFullYear())}
            </p>
        </div>
  )
}

export default Footer