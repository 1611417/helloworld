import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import styles from '../About/About.module.css'



const About = function(props){
    return(
        <div className="container">
            <h1 className={styles.about_titre}>About</h1>
            <Row className={styles.row_about}>
                <Col lg={6} className={styles.img_react}></Col>
                <Col lg={6}>
                    <h1>React</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt magni facilis deleniti quos laborum, dolor delectus debitis cupiditate asperiores dolorem voluptas autem, quasi laudantium consequatur natus libero eaque nesciunt doloribus! Quaerat pariatur temporibus distinctio sapiente voluptatibus odio fugiat! Est, eius? Rerum consectetur dolorem voluptas iusto, aspernatur quod accusamus eos aut quas, minus ipsum amet delectus rem iure commodi beatae nisi temporibus suscipit mollitia voluptates sed placeat obcaecati nulla! Cupiditate corrupti iure dignissimos sit laborum minima dolorem asperiores ullam veniam labore temporibus molestias enim, repellat, accusantium ducimus facere culpa est. Sint, cum. Nemo adipisci, vel iste libero nobis ipsam saepe explicabo.</p>
                </Col>
            </Row>
            <Row className={styles.row_about}>
                <Col lg={6}>
                    <h1>Bootstrap</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt magni facilis deleniti quos laborum, dolor delectus debitis cupiditate asperiores dolorem voluptas autem, quasi laudantium consequatur natus libero eaque nesciunt doloribus! Quaerat pariatur temporibus distinctio sapiente voluptatibus odio fugiat! Est, eius? Rerum consectetur dolorem voluptas iusto, aspernatur quod accusamus eos aut quas, minus ipsum amet delectus rem iure commodi beatae nisi temporibus suscipit mollitia voluptates sed placeat obcaecati nulla! Cupiditate corrupti iure dignissimos sit laborum minima dolorem asperiores ullam veniam labore temporibus molestias enim, repellat, accusantium ducimus facere culpa est. Sint, cum. Nemo adipisci, vel iste libero nobis ipsam saepe explicabo.</p>
               </Col>
                <Col lg={6} className={styles.img_bootstrap}></Col>
            </Row>
            <Row className={styles.row_about}>
                <Col lg={6} className={styles.img_express}></Col>
                <Col lg={6}>
                    <h1>Node JS - Express</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt magni facilis deleniti quos laborum, dolor delectus debitis cupiditate asperiores dolorem voluptas autem, quasi laudantium consequatur natus libero eaque nesciunt doloribus! Quaerat pariatur temporibus distinctio sapiente voluptatibus odio fugiat! Est, eius? Rerum consectetur dolorem voluptas iusto, aspernatur quod accusamus eos aut quas, minus ipsum amet delectus rem iure commodi beatae nisi temporibus suscipit mollitia voluptates sed placeat obcaecati nulla! Cupiditate corrupti iure dignissimos sit laborum minima dolorem asperiores ullam veniam labore temporibus molestias enim, repellat, accusantium ducimus facere culpa est. Sint, cum. Nemo adipisci, vel iste libero nobis ipsam saepe explicabo.</p>
               </Col>
            </Row>
            <Row className={styles.row_about}>
                <Col lg={6}>
                    <h1>Mongo DB</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt magni facilis deleniti quos laborum, dolor delectus debitis cupiditate asperiores dolorem voluptas autem, quasi laudantium consequatur natus libero eaque nesciunt doloribus! Quaerat pariatur temporibus distinctio sapiente voluptatibus odio fugiat! Est, eius? Rerum consectetur dolorem voluptas iusto, aspernatur quod accusamus eos aut quas, minus ipsum amet delectus rem iure commodi beatae nisi temporibus suscipit mollitia voluptates sed placeat obcaecati nulla! Cupiditate corrupti iure dignissimos sit laborum minima dolorem asperiores ullam veniam labore temporibus molestias enim, repellat, accusantium ducimus facere culpa est. Sint, cum. Nemo adipisci, vel iste libero nobis ipsam saepe explicabo.</p>
                </Col>
                <Col lg={6} className={styles.img_mongodb}></Col>
            </Row>
        </div>
    )
}

export default About