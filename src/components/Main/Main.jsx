import styles from './Main.module.css'
import ShopList from '../ShopList/ShopList'
import RecettesMain from '../Recettes_Main/RecettesMain'
import Home from '../Home/Home'
import About from '../About/About'
import Favorites from '../Favorites/Favorites'
import Historique from '../Historique/Historique'
import Login from '../Login/Login'
import AddRecette from '../AddRecette/AddRecette'
import RecetteAffiche from '../RecetteAffiche/RecetteAffiche'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import {Route} from 'react-router-dom'
import Basket from '../ShopListredux/Basket'
import { connect } from 'react-redux'


const Main = function(props){
    const {meals, urlId} = props;
    const recettePath = "/recette/"+urlId
    const {activeUser} = props;

    console.log("urlId :");
    console.log(urlId);

    const manageLogin = () => {
        if(!activeUser){
            return (
                <div>
                <Route path="/">
                    <Login />
                </Route>
                </div>
            )
        } else{
            return (
                <Row noGutters="true">
                <Col lg={2}></Col>
                <Col lg={8}>                    
            
                    <Route exact path="/home">
                        <Home />
                    </Route>
                    <Route exact path="/about">
                        <About />
                    </Route>
                    <Route exact path="/recettes">
                    <RecettesMain />
                    </Route>
                    <Route path="/favorites">
                        <Favorites />
                    </Route>
                    <Route exact path="/historique">
                        <Historique />
                    </Route>
                    <Route path="/login">
                        <Login />
                    </Route>
                    <Route exact path="/addrecette">
                        <AddRecette />
                    </Route>
                    <Route path="/recette/:id" component={RecetteAffiche}>
                    </Route>
                </Col>
                <Col lg={2} className={styles.shoplist}>
                    <Basket />
                </Col>
            </Row>
            )
            
        }
    }

    return(
        <div className={styles.main}>
            {manageLogin()}
            {/* <Row noGutters="true">
                <Col lg={2}></Col>
                <Col lg={8}>                    
            
                    <Route exact path="/home">
                        <Home />
                    </Route>
                    <Route exact path="/about">
                        <About />
                    </Route>
                    <Route exact path="/recettes">
                    <RecettesMain />
                    </Route>
                    <Route exact path="/favorites">
                        <Favorites />
                    </Route>
                    <Route exact path="/historique">
                        <Historique />
                    </Route>
                    <Route exact path="/login">
                        <Login />
                    </Route>
                    <Route exact path="/addrecette">
                        <AddRecette />
                    </Route>
                    <Route path="/recette/:id" component={RecetteAffiche}>
                    </Route>
                </Col>
                <Col lg={2} className={styles.shoplist}>
                    <Basket />
                </Col>
            </Row> */}
        </div>

    )

}

const mapStateToProps  = (state) => ({
    meals : state.MealReducer.meals,
    urlId : state.MealReducer.urlId,
    activeUser : state.UserReducer.activeUser
})

export default connect(mapStateToProps)(Main)