import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Navigation from './components/Header/Header'
import Main from './components/Main/Main'
import Footer from './components/Footer/Footer'




function App() {
  return (
    <div className="App">

     {/* 
      if activeUser !== null
     */} 
    <Navigation></Navigation>
    <Main></Main>
    <Footer></Footer>

    {/*
    else{
      <Login>
    } */}
    </div>
  );
}

export default App;
